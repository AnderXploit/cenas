# Top GAN Generator
**Fully automatically generate numerous XSS payloads for web application assessment.**

## Installation

### Step.1 Install required python packages.
```
pip -V
pip install -r requirements.txt
```

### Step.2 Get the web driver for selenium.
[!] Use the Firefox driver.  

firefox driver: https://github.com/mozilla/geckodriver/releases  

And you have to move downloaded driver file to `web_drivers` directory.  




## Operation check environment
* Hardware  
  * OS: Windows 10  
  * CPU: Intel(R) Core(TM) i7-6500U 2.50GHz  
  * GPU: None  
  * Memory: 8.0GB  
* Software  
  * Python 3.6.0 
  * Keras==2.1.6  
  * numpy==1.13.3  
  * pandas==0.23.0  
  * selenium==3.14.0  

