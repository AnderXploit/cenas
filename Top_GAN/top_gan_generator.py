# -*- coding: utf-8 -*-
import os
import sys
import configparser
from selenium import webdriver
from util import Utilty
from ga_main import GeneticAlgorithm

# Type of printing.
OK = 'ok'         # [*]
NOTE = 'note'     # [+]
FAIL = 'fail'     # [-]
WARNING = 'warn'  # [!]
NONE = 'none'     # No label.


def banner():    
    ascii_art = '''


`-:-.   ,-;"`-:-.   ,-;"`-:-.   ,-;"`-:-.   ,-;"
   `=`,'=/     `=`,'=/     `=`,'=/     `=`,'=/
     y==/        y==/        y==/        y==/
   ,=,-<=`.    ,=,-<=`.    ,=,-<=`.    ,=,-<=`.
,-'-'   `-=_,-'-'   `-=_,-'-'   `-=_,-'-'   `-=_


'''
    return ascii_art



if __name__ == "__main__":
    print(banner())
    util = Utilty()

    # Read config.ini.
    full_path = os.path.dirname(os.path.abspath(__file__))
    config = configparser.ConfigParser()
    try:
        config.read(util.join_path(full_path, 'config.ini'))
    except FileExistsError as e:
        util.print_message(FAIL, 'File exists error: {}'.format(e))
        sys.exit(1)

    # Genetic Algorithm setting value.
    max_try_num = int(config['Genetic']['max_try_num'])

    # Selenium setting value.
    driver_dir = util.join_path(full_path, config['Selenium']['driver_dir'])
    driver_list = config['Selenium']['driver_list'].split('@')
    window_width = int(config['Selenium']['window_width'])
    window_height = int(config['Selenium']['window_height'])
    position_width = int(config['Selenium']['position_width'])
    position_height = int(config['Selenium']['position_height'])

    # Start revolution using each browser.
    for browser in driver_list:
        # Create Web driver.
        if 'geckodriver' in browser:
            obj_browser = webdriver.Firefox(executable_path=util.join_path(driver_dir, browser))
            util.print_message(NOTE, 'Launched: {} {}'.format(obj_browser.capabilities['browserName'],
                                                               obj_browser.capabilities['browserVersion']))
        else:
            util.print_message(FAIL, 'Invalid browser driver : {}'.format(browser))
            sys.exit(1)

        # Browser setting.
        obj_browser.set_window_size(window_width, window_height)
        obj_browser.set_window_position(position_width, position_height)

        # Create a few individuals from gene list.
        current_results = set()
        current_encoded_results = set()
        double_current_encoded_results = set()
        for idx in range(max_try_num):
            util.print_message(NOTE, '{}/{} Create individuals using Genetic Algorithm.'.format(idx + 1, max_try_num))
            ga = GeneticAlgorithm(obj_browser)
            payloads, encoded_payloads, double_encoded_payloads = ga.main(current_results, current_encoded_results, double_current_encoded_results)
            current_results = current_results.union(payloads)
            current_encoded_results = current_encoded_results.union(encoded_payloads)
            double_current_encoded_results = double_current_encoded_results.union(double_encoded_payloads)
        
        # Close browser.
        obj_browser.close()
