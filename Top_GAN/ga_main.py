# -*- coding: utf-8 -*-
import os
import sys
import random
import re
import codecs
import subprocess
import locale
import configparser
import pandas as pd
import datetime
from decimal import Decimal
from util import Utilty
import csv

import urllib.parse

from tidylib import Tidy

# Type of printing.
OK = 'ok'         # [*]
NOTE = 'note'     # [+]
FAIL = 'fail'     # [-]
WARNING = 'warn'  # [!]
NONE = 'none'     # No label.


# Container of genes.
class Gene:
    genom_list = None
    evaluation = None

    def __init__(self, genom_list, evaluation):
        self.genom_list = genom_list
        self.evaluation = evaluation

    def getGenom(self):
        return self.genom_list

    def getEvaluation(self):
        return self.evaluation

    def setGenom(self, genom_list):
        self.genom_list = genom_list

    def setEvaluation(self, evaluation):
        self.evaluation = evaluation


# Genetic Algorithm.
class GeneticAlgorithm:
    def __init__(self, browser):
        self.util = Utilty()
        self.obj_browser = browser

        # Read config.ini.
        full_path = os.path.dirname(os.path.abspath(__file__))
        config = configparser.ConfigParser()
        try:
            config.read(self.util.join_path(full_path, 'config.ini'))
        except FileExistsError as e:
            self.util.print_message(FAIL, 'File exists error: {}'.format(e))
            sys.exit(1)
        # Common setting value.
        self.html_dir = self.util.join_path(full_path, config['Common']['html_dir'])
        self.html_file = config['Common']['ga_html_file']
        self.result_dir = self.util.join_path(full_path, config['Common']['result_dir'])

        # Genetic Algorithm setting value.
        self.genom_length = int(config['Genetic']['genom_length'])
        self.max_genom_list = int(config['Genetic']['max_genom_list'])
        self.select_genom = int(config['Genetic']['select_genom'])
        self.individual_mutation_rate = float(config['Genetic']['individual_mutation_rate'])
        self.genom_mutation_rate = float(config['Genetic']['genom_mutation_rate'])
        self.max_generation = int(config['Genetic']['max_generation'])
        self.max_fitness = int(config['Genetic']['max_fitness'])
        self.gene_dir = self.util.join_path(full_path, config['Genetic']['gene_dir'])
        self.genes_path = self.util.join_path(self.gene_dir, config['Genetic']['gene_file'])
        self.html_checked_path = self.util.join_path(self.html_dir, config['Genetic']['html_checked_file'])
        self.bingo_score = float(config['Genetic']['bingo_score'])
        self.warning_score = float(config['Genetic']['warning_score'])
        self.error_score = float(config['Genetic']['error_score'])
        self.result_file = config['Genetic']['result_file']
        self.result_list = set()
        self.encoded_result_list = set()
        self.double_encoded_result_list = set()

    # Create population.
    def create_genom(self, df_gene):
        lst_gene = []
        for _ in range(self.genom_length):
            lst_gene.append(random.randint(0, len(df_gene.index)-1))
        self.util.print_message(OK, 'Created individual : {}.'.format(lst_gene))
        return Gene(lst_gene, 0)

    # Evaluation.
    def evaluation(self, obj_ga, df_gene, individual_idx):
        # Build html syntax.
        individual = self.util.transform_gene_num2str(df_gene, obj_ga.genom_list)
        encoded_individual = urllib.parse.quote(individual)
        double_encoded_individual = urllib.parse.quote(encoded_individual)

        eval_html_path = self.util.join_path(self.html_dir, self.html_file.replace('*', str(individual_idx)))
        
        with codecs.open(eval_html_path, 'w', encoding='utf-8') as fout:
            fout.write(individual)
                
        tidy = Tidy()
        document, infos = tidy.tidy_document(individual,
            options={"tidy-mark": 1, "indent": 1,})

        erros = open(self.html_checked_path, "w")
        erros.write(infos)
        erros.close()

        
        # Check html checked result.
        str_eval_result = ''
        with codecs.open(self.html_checked_path, 'r', encoding='utf-8') as fin:
            str_eval_result = fin.read()

        # Check warning and error number.           
        n_warnings = len(re.findall(r'(?=Warning:)', infos))
        n_errors = len(re.findall(r'(?=Error:)', infos))
             
        warnings = 0.0
        errors = 0.0
        if n_warnings !=0:
            warnings = n_warnings * self.warning_score
        elif n_errors !=0:
            errors = n_errors * self.error_score
        else:
            return None, 1
        
        # Compute score.
        int_score = warnings + errors


        # Evaluate running script using selenium.
        selenium_score, error_flag = self.util.check_individual_selenium(self.obj_browser, individual)
        
        # Evaluate the running encoded script using selenium
        encoded_selenium_score, encoded_error_flag = self.util.check_individual_selenium(self.obj_browser, encoded_individual)

        double_encoded_selenium_score = -1
        double_encoded_error_flag = False
        if individual_idx < 30:
            double_encoded_selenium_score, double_encoded_error_flag = self.util.check_individual_selenium(self.obj_browser, double_encoded_individual)

        if error_flag and encoded_error_flag and double_encoded_error_flag:
            return None, 1

        # Check result of selenium.
        if selenium_score > 0 or encoded_selenium_score > 0 or double_encoded_selenium_score > 0:
            self.util.print_message(NOTE, '--->>> Detect running script: {}'.format(individual))

            # compute score for running script.
            int_score += self.bingo_score
            if selenium_score > 0:
                self.result_list.add(tuple(obj_ga.genom_list))
            
            if encoded_selenium_score > 0:
                self.encoded_result_list.add(tuple(obj_ga.genom_list))

            if double_encoded_selenium_score > 0:
                self.double_encoded_result_list.add(tuple(obj_ga.genom_list))
             
            # Output evaluation results.
            self.util.print_message(OK, 'Evaluation result : Browser={} {}, '
                                        'Individual="{} ({})", '
                                        'Score={}'.format(self.obj_browser.name,
                                                          self.obj_browser.capabilities['browserVersion'],
                                                          individual,
                                                          obj_ga.genom_list,
                                                          str(int_score)))
        return int_score, 0

    # Select elite individual.
    def select(self, obj_ga, elite):
        # Sort in desc order of evaluation.
        sort_result = sorted(obj_ga, reverse=True, key=lambda u: u.evaluation)

        # Extract elite individuals.
        return [sort_result.pop(0) for _ in range(elite)]

    # Crossover (create offspring).
    def crossover(self, ga_first, ga_second):
        genom_list = []

        # Setting of two-point crossover.
        cross_first = random.randint(0, self.genom_length)
        cross_second = random.randint(cross_first, self.genom_length)
        one = ga_first.getGenom()
        second = ga_second.getGenom()

        # Crossover.
        progeny_one = one[:cross_first] + second[cross_first:cross_second] + one[cross_second:]
        progeny_second = second[:cross_first] + one[cross_first:cross_second] + second[cross_second:]
        genom_list.append(Gene(progeny_one, 0))
        genom_list.append(Gene(progeny_second, 0))

        return genom_list

    # Create population of next generation.
    def next_generation_gene_create(self, ga, ga_progeny):
        # Sort in asc order of evaluation.
        next_generation_geno = sorted(ga, reverse=False, key=lambda u: u.evaluation)

        # Remove sum of adding the offspring group.
        for _ in range(0, len(ga_progeny)):
            next_generation_geno.pop(0)

        # Add the offspring group to the next generation.
        next_generation_geno.extend(ga_progeny)
        return next_generation_geno

    # Mutation.
    def mutation(self, obj_ga, induvidual_mutation, genom_mutation, df_genes):
        lst_ga = []
        for idx in obj_ga:
            # Mutation to individuals.
            if induvidual_mutation > (random.randint(0, 100) / Decimal(100)):
                lst_gene = []
                for idx2 in idx.getGenom():
                    # Mutation to genes.
                    if genom_mutation > (random.randint(0, 100) / Decimal(100)):
                        lst_gene.append(random.randint(0, len(df_genes.index)-1))
                    else:
                        lst_gene.append(idx2)
                idx.setGenom(lst_gene)
                lst_ga.append(idx)
            else:
                lst_ga.append(idx)
        return lst_ga

    # Main control.
    def main(self, current_results, current_encoded_results, current_double_encoded_results):
        # Load gene list.
        df_genes = pd.read_csv(self.genes_path, encoding='utf-8').fillna('')

        # Create saving file (only header).
        time =  datetime.datetime.now()
        save_path = self.util.join_path(self.result_dir, self.result_file.replace('*', '{}_{}'.format(time.strftime("%d-%m"), time.strftime("%Hh%M"))))        
        pd.DataFrame([], columns=['time', 'genom', 'bool_encoded', 'payload']).to_csv(save_path, mode='w', header=True, index=False)

        self.util.print_message(NOTE, 'Genetic Algorithm START')

        # Generate 1st generation.
        self.util.print_message(NOTE, 'Create population.')
        current_generation = []
        for _ in range(self.max_genom_list):
            current_generation.append(self.create_genom(df_genes))

        # Evaluate each generation.
        for int_count in range(1, self.max_generation + 1):
            self.util.print_message(NOTE, 'Evaluate individual : {}/{} generation.'.format(str(int_count),
                                                                                               self.max_generation))
            for individual, idx in enumerate(range(self.max_genom_list)):
                self.util.print_message(OK, 'Evaluation individual '
                                                '{}/{} in {} generation: {}'.format(individual + 1,
                                                                                self.max_genom_list,
                                                                                str(int_count), self.util.transform_gene_num2str(df_genes, current_generation[individual].genom_list)))
                evaluation_result, eval_status = self.evaluation(current_generation[individual],
                                                                     df_genes,
                                                                     idx)

                idx += 1
                if eval_status == 1:
                    individual -= 1
                    continue
                current_generation[individual].setEvaluation(evaluation_result)


            # Arrange fitness each individual.
            fits = [_.getEvaluation() for _ in current_generation]

            # evaluate evolution result.
            flt_avg = sum(fits) / float(len(fits))
            self.util.print_message(NOTE, '{} generation result: '
                                             'Min={}, Max={}, Avg={}.'.format(int_count,
                                                                               min(fits),
                                                                               max(fits),
                                                                               flt_avg))


            # Save individuals.
            output = []
            time = datetime.datetime.now()
            for genom in self.result_list:
                if genom not in current_results:
                    current_results.add(genom)
                    text = self.util.transform_gene_num2str(df_genes, genom)
                    output.append(['Time: {}'.format(time.strftime("%H:%M")), 'Regular', ' '.join(map(str, genom)), text])
            self.result_list.clear();
            
            for genom in self.encoded_result_list:
                if genom not in current_encoded_results:
                    current_encoded_results.add(genom)
                    text = self.util.transform_gene_num2str(df_genes, genom)
                    output.append(['Time: {}'.format(time.strftime("%H:%M")), 'Encoded', ' '.join(map(str, genom)), urllib.parse.quote(text)])
            self.encoded_result_list.clear();
            
            for genom in self.double_encoded_result_list:
                if genom not in current_double_encoded_results:
                    current_double_encoded_results.add(genom)
                    text = self.util.transform_gene_num2str(df_genes, genom)
                    output.append(['Time: {}'.format(time.strftime("%H:%M")), '2x Encoded', ' '.join(map(str, genom)), urllib.parse.quote(text)])
            self.double_encoded_result_list.clear();
            
            pd.DataFrame(output).to_csv(save_path, mode='a', header=False, index=False, quoting=csv.QUOTE_NONE, quotechar="", sep="\t")



            # Select elite's individual.
            elite_genes = self.select(current_generation, self.select_genom)

            # Judge fitness.
            if flt_avg > self.max_fitness:
                self.util.print_message(NOTE, 'Finish evolution: average={}'.format(str(flt_avg)))
                break

            # Crossover of elite gene.
            progeny_gene = []
            for i in range(0, self.select_genom):
                progeny_gene.extend(self.crossover(elite_genes[i - 1], elite_genes[i]))

            # Select elite group.
            next_generation_individual_group = self.next_generation_gene_create(current_generation,
                                                                                    progeny_gene)

            # Mutation
            next_generation_individual_group = self.mutation(next_generation_individual_group,
                                                                 self.individual_mutation_rate,
                                                                 self.genom_mutation_rate,
                                                                 df_genes)

            

            # Finish evolution computing for current generation.   
            # Replace current generation and next generation.
            current_generation = next_generation_individual_group
                                
            

        # Output final result.
        str_best_individual = ''
        for gene_num in elite_genes[0].getGenom():
            str_best_individual += str(df_genes.loc[gene_num].values[0])
        str_best_individual = str_best_individual.replace('%s', ' ').replace('&quot;', '"').replace('%comma', ',')
        self.util.print_message(NOTE, 'Best individual : {}'.format(str_best_individual))
        self.util.print_message(NOTE, 'Done creation of injection codes using Genetic Algorithm.')

        return current_results, current_encoded_results, current_double_encoded_results
